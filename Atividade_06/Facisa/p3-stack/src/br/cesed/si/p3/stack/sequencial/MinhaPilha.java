public class MinhaPilha {

	private static final int TAMANHO_INICIAL = 3;
	
	private Object[] pilha = new Object[TAMANHO_INICIAL];

	private int topo = -1;
	
	public void push(Object objeto) {
		
		if(pilha.length == topo+1) {
			Object[] pilhaMaior = new Object[pilha.length + TAMANHO_INICIAL];
			for(int i = 0; i< pilha.length; i++) {
				pilha[i] = pilhaMaior[i];
			}
			pilha = pilhaMaior;
		 }
		 if(objeto != null) {
			pilha[++topo] = objeto;
			
		 }
		
	}
	
	public Object pop() {
		
		Object fora = null;
		
		if(size()!= 0) {
			fora = pilha[topo];
			pilha[topo] = null;
			topo --;
			
		}
		
		return fora;
	}

	public Object top() {
		return pilha[topo];
	}
	
	public int size() {
		return topo + 1;
	}

	public boolean isEmpty() {
		return (topo<0);
	}
	
	public void imprimir() {
		
	}
}
