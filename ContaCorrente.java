package Facisa_p3;

public class ContaCorrente extends ContaPoupanca{


	public ContaCorrente(int agencia, int conta, String titular, int limite, double saldo, double valorLimite) {
		super(agencia, conta, titular, limite, saldo, valorLimite);
	}

	//Utilizando a sobrescrita
	@Override
	public final double getdepositar(double valor) {
		saldo --;
		return saldo;
	}
}
