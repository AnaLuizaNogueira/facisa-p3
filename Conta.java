package Facisa_p3;

public class Conta {
	
	public int agencia;
	public int conta;
	public String titular;
	public int limite;
	public double saldo;
	public double valorLimite;
	private int senha;
	
	public Conta(int agencia, int conta, String titular, int limite, double saldo, double valorLimite) {
		super();
		this.agencia = agencia;
		this.conta = conta;
		this.titular = titular;
		this.limite = limite;
		this.saldo = saldo;
		this.valorLimite = valorLimite;
	}
	
	//Metodo utilizando sobrecarga 
	public void sacar(double valor,int senha) {
		if(senha == this.senha) {
		  this.saldo -= valor; 
		}
	}
	
	public void sacar(double valor) {
		if(valor<=this.valorLimite) {
			this.saldo -= valor;
		}else {
			System.out.println("Valor maior do que o limite");
		}
	}
	
	public void depositar(double valor) {
		this.saldo += valor; 
	}
	
	public int getAgencia() {
		return agencia;
	}
	public void setAgencia(int agencia) {
		this.agencia = agencia;
	}
	public int getConta() {
		return conta;
	}
	public void setConta(int conta) {
		this.conta = conta;
	}
	public String getTitular() {
		return titular;
	}
	public void setTitular(String titular) {
		this.titular = titular;
	}
	public int getLimite() {
		return limite;
	}
	public void setLimite(int limite) {
		this.limite = limite;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public double getValorLimite() {
		return valorLimite;
	}
	public void setValorLimite(double valorLimite) {
		this.valorLimite = valorLimite;
	}
	

}
