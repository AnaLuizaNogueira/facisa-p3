package Facisa_p3;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ContaTest {
	
	@Test
	public void TestDeposito() {
		
		Conta conta = new Conta(12,13213,"Beltrano",100,200,100);
		conta.depositar(100);
		assertEquals(conta.getSaldo(),300,0);
	}
	
	@Test
	public void TestDepositoNegativo() {
		
		Conta conta = new Conta(12,13213,"Beltrano",100,200,100);
		conta.depositar(-100);
		assertEquals(conta.getSaldo(),100,0);
	}
	
	@Test
	public void TestDepositoNulo() {
		
		Conta conta = new Conta(12,13213,"Beltrano",100,200,100);
		conta.depositar(0);
		assertEquals(conta.getSaldo(),200,0);
	}
	
	@Test
	public void TestSaque() {
		
		Conta conta = new Conta(12,13213,"Beltrano",100,200,200);
		conta.sacar(200);
		assertEquals(conta.getSaldo(),0,0);
	}
	
	@Test
	public void TestSaqueNegativo() {
			Conta conta = new Conta(12,13213,"Beltrano",100,200,100);
			conta.depositar(-100);
			assertEquals(conta.getSaldo(),100,0);
		}

	}


