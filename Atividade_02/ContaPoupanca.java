package Facisa_p3;

//Classe abstrata n�o pode ser instanciada
public abstract class ContaPoupanca extends Conta {

	public ContaPoupanca(int agencia, int conta, String titular, int limite, double saldo, double valorLimite) {
		super(agencia, conta, titular, limite, saldo, valorLimite);
	}
	
	
	//Metodo abstrato, s� possui o comportamento
	public abstract double getdepositar(double valor);
}


