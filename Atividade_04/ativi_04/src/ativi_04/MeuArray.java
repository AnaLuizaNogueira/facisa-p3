package ativi_04;

public class MeuArray {


	private static final int TAM = 3;
	private int inseridos;
    private Object[] arrayInterno = new Object[TAM];
	
    //Insere objeto no array
	public void inserir(Object inserir) {

		//Caso o n�mero de objetos seja maior que o tamanho,
		//o tamanho do array ir� ser acrescido de 3, at� 
		//suprir a nescessidade de objetos inseridos
		if (inseridos == arrayInterno.length) {

			Object arrayMaior[] = new Object[arrayInterno.length + TAM];

			for (int i = 0; i < arrayInterno.length; i++) {
				arrayMaior[i] = arrayInterno[i];
			}
			arrayInterno = arrayMaior;
		}
		//S� insere no array se o objeto for diferente de null
        if(inserir != null) {
        	arrayInterno[inseridos] = inserir;
    		inseridos ++;
        }
		
	}
	
	//Metodo para remover determinado objeto do array
	public void remover(Object valor) {
		int valorEncontrado = -1;
		
        //Achar se o objeto valor possui no array
		for(int i = 0;i< arrayInterno.length; i++) {
			
			if(valor.equals(arrayInterno[i])) {
				
				valorEncontrado = i;
				break;
			}
			try{
			 valorEncontrado = (Integer) null;
			}
			catch(Exception e){
			  System.out.println("Valor invalido");
			}
		}
		
        //Se encontrar o objeto, sera removido do array e o array
		//ser� reorganizado. Como tamb�m o valor de inserido ser�
		//diminuido
		if(valorEncontrado != -1) {
			
			for(int i1 = valorEncontrado; i1 < inseridos-1; i1++) {
				
				arrayInterno[i1] = arrayInterno[i1+1];
			}
			arrayInterno[inseridos-1] = null;
			inseridos --;
		}

		
    }

	public int tamanho() {

		return inseridos;
	}

}


