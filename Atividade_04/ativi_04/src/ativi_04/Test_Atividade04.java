package ativi_04;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class Test_Atividade04 {
	
	@Test
	public void dobrarArray() {
		MeuArray a1 = new MeuArray();
		a1.inserir("f");
		a1.inserir("h");
	    a1.inserir("v");
	    a1.inserir("i");
	    assertEquals(4,a1.tamanho());
	}
	
	@Test
	public void arrayNullo() {
		MeuArray ma = new MeuArray();
		ma.inserir(null);
	    assertEquals(0,ma.tamanho());
	}
	
	@Test
	public void removerPosicaoInicial() {
		MeuArray l = new MeuArray();
		l.inserir("f");
		l.inserir("h");
	    l.remover("f");
	    assertEquals(1,l.tamanho());
	}
	
	@Test
	public void removerPosicaoMedia() {
		MeuArray l = new MeuArray();
		l.inserir("f");
		l.inserir("h");
		l.inserir("m");
	    l.remover("h");
	    assertEquals(2,l.tamanho());
	
   }
	@Test
	public void removerObjetoInvalido() {
		MeuArray l = new MeuArray();
		l.inserir("f");
		l.inserir("h");
		l.inserir("m");
	    l.remover("i");
	    assertEquals(3,l.tamanho());
	
   }
	
   }
