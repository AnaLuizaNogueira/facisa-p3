package br.com.itau.estruturaDados;

public class FilaBancaria {

	private MinhaFila filaNaoPreferencial = new MinhaFila();
	private MinhaFila filaPreferencial = new MinhaFila();
	private Object objeto;
	int contador = 0;
	int var = 0;
	private int m;
	
	public void enqueue(Object objeto, boolean ehPreferencial) {
	
		if(ehPreferencial) {
			filaPreferencial.enqueue(objeto);
		} else {
			filaNaoPreferencial.enqueue(objeto);
		}
	}
	
	public Object dequeue() {
	
	if(contador == 0) {
		if(filaPreferencial.tamanho()!= 0) {
			filaPreferencial.dequeue();
			contador = 1;
			m = 3;
		}
		contador = 1;
	} 
	if(contador == 1) {
		if( filaNaoPreferencial.tamanho()!=0 && var<2 && m!=3) {
		 filaNaoPreferencial.dequeue();
		 var++;
		}
	    if(var==2){
		 contador = 0;
		 var = 0;
	    }
	    if(filaNaoPreferencial.tamanho() == 0){
	    	 contador = 0;
	    }
	    m = 0;
		
	}

				
	return objeto;
	}
		
		
	
	public boolean isEmpty() {
		return filaNaoPreferencial.isEmpty() && filaPreferencial.isEmpty();
	}
	
	public int tamanho() {
		return filaNaoPreferencial.tamanho() + filaPreferencial.tamanho();
	}	
	
	
}
