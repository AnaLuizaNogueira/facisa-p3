package br.com.itau.estruturaDados;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

class TesteFilaBancaria {
	
	@Test
	void testDobrarFila() {
		
		FilaBancaria m = new FilaBancaria();
	    m.enqueue("b", true);
	    m.enqueue("m", false);
	    m.enqueue("b", true);
	    m.enqueue("b", true);
	    m.enqueue("b", true);
	    
	    assertEquals(5,m.tamanho());
	}
	
	@Test
	void testDesempilhar() {
		
		FilaBancaria m = new FilaBancaria();
	    m.enqueue("b", true);
	    m.enqueue("m", false);
	    m.enqueue("b", true);
	    m.enqueue("b", true);
	    m.dequeue();
	    m.dequeue();
	    assertEquals(2,m.tamanho());
	}
	
	@Test
    void testPreferencialNulo() {
		
		FilaBancaria m = new FilaBancaria();
	    m.enqueue("m", false);
	    m.enqueue("m", false);
	    m.enqueue("m", false);
	    m.dequeue();
	    m.dequeue();
	    m.dequeue();
	    assertEquals(0,m.tamanho());
	}
	
	@Test
	void testNaoPreferencialNulo() {
		
		FilaBancaria m = new FilaBancaria();
	    m.enqueue("m", true);
	    m.enqueue("m", true);
	    m.dequeue();
	    m.dequeue();
	    assertEquals(0,m.tamanho());
	}
	
	@Test
	void testNaoPreferencial() {
		
		FilaBancaria m = new FilaBancaria();
	    m.enqueue("m", true);
	    m.enqueue("k", true);
	    m.enqueue("l", false);
	    m.dequeue();
	    m.dequeue();
	    assertEquals(1,m.tamanho());
	}
	
	@Test
	void testDequeue() {
		FilaBancaria m = new FilaBancaria();
	    m.enqueue("m", false);
	    m.enqueue("m", false);
	    m.enqueue("m", false);
	    m.enqueue("m", false);
	    m.enqueue("m", false);
	    m.enqueue("d", true);
	    m.dequeue();
	    m.dequeue();
	    m.dequeue();
	    assertEquals(3,m.tamanho());
		
	}
}
