package br.com.itau.estruturaDados;

public class MinhaFila {

	private static final int ALOCACAO = 3;
	
	private Object[] arrayInterno = new Object[ALOCACAO];

	private int inseridos;

	
	public void enqueue(Object objeto) {
		
		if(objeto == null) {
			throw new IllegalArgumentException();
		}
			
		if(inseridos == arrayInterno.length) {
			
			Object[] arrayTemporario = (Object[]) new Object[arrayInterno.length + ALOCACAO];
			
			for (int i = 0; i < arrayInterno.length; i++) {
				arrayTemporario[i] = arrayInterno[i];
			}
			arrayInterno = arrayTemporario;
		}
		
		arrayInterno[inseridos] = objeto;
		inseridos++;
		System.out.println("entrouuuuuuuuuuuuuuuuuuuuuuuuu");
	}
	
	
	public Object dequeue() {
		if(isEmpty()) {
			return null;
 		}
		
		Object object = arrayInterno[0]; 
		arrayInterno[0] = null;
		inseridos --;
		
		for(int i = 0; i < inseridos - 1; i++) {
			arrayInterno[i] = arrayInterno[i+1];
			
		}
		
		return object;
	
	}
	
	public boolean isEmpty() {
		return inseridos == 0;
	}
	
	public int tamanho() {
		return inseridos;
	}
}